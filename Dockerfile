FROM openjdk:8-jdk-alpine
ARG JAR_FILE
COPY ${JAR_FILE} floow-restapi.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/floow-restapi.jar"]