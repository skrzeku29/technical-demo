# Technical test

This application is a spring boot rest api that can be run with local instance of mongodb or run spring app and mongo as two docker containers.

##Running the application with local mongodb running in the background
- requires jdk8+, mongodb running with open access and apache maven installed
```
#cd into the of project directory (pom.xml level)

mvn spring-boot:run
```
##Running application via docker-compose
- requires docker, docker-compose and apache maven installed
```
#cd into the of project directory (pom.xml level)

mvn clean install
docker-compose up
```

## Testing of the api
to test the api use terminal with curl or postman and hit localhost:8080 with URIs given in technical sheet.

- /drivers - GET request returning JSON of drivers in a database
- /drivers/byDate?date=<date> - GET request returning Json of drivers in database after the given date in yyyy-MM-dd format
- /driver/create - POST request requiring a json in a body to save a driver to a database
e.g  
```
{
     "firstname": "John",
     "lastname": "Smith",
     "date_of_birth": "1980-03-22"
 }
```