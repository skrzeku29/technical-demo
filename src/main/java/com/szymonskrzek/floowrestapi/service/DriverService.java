package com.szymonskrzek.floowrestapi.service;

import com.szymonskrzek.floowrestapi.model.Driver;
import com.szymonskrzek.floowrestapi.repository.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DriverService {

    private DriverRepository driverRepository;

    @Autowired
    public DriverService(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    public List<Driver> findAllDrivers() {
        return driverRepository.findAll();
    }

    public void saveDriver(Driver driver) {
        driverRepository.save(driver);
    }

    public List<Driver> findAllDriversCreatedAfterDate(Calendar date) {
        return findAllDrivers().stream()
                .filter(d -> d.getCreation_date().after(date))
                .collect(Collectors.toList());
    }
}
