package com.szymonskrzek.floowrestapi.model;

import org.springframework.data.annotation.Id;

import java.util.Calendar;

public class Driver {

    @Id
    private String id;

    private String firstname;
    private String lastname;
    private Calendar date_of_birth;
    private Calendar creation_date;

    public Driver(String firstname, String lastname, Calendar date_of_birth) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.date_of_birth = date_of_birth;
        this.creation_date = Calendar.getInstance();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Calendar getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Calendar date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public Calendar getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Calendar creation_date) {
        this.creation_date = creation_date;
    }

    @Override
    public String toString() {
        return String.format(
                "Driver[id=%s, firstName='%s', lastName='%s', date_of_birth='%s', creationDate='%s']",
                id, firstname, lastname, date_of_birth.getTime(),  creation_date.getTime());
    }
}
