package com.szymonskrzek.floowrestapi.repository;

import com.szymonskrzek.floowrestapi.model.Driver;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DriverRepository extends MongoRepository<Driver, String> {
}
