package com.szymonskrzek.floowrestapi.controller;

import com.szymonskrzek.floowrestapi.model.Driver;
import com.szymonskrzek.floowrestapi.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@RestController
public class DriverController {
    private DriverService driverService;

    @Autowired
    public DriverController(DriverService driverService) {
        this.driverService = driverService;
    }

    @GetMapping("/drivers")
    public List<Driver> getAllDrivers(){
        return driverService.findAllDrivers();
    }

    @GetMapping("drivers/byDate")
    public List<Driver> getDriverByDate(@RequestParam(name = "date") String creation_date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar date = Calendar.getInstance();
        date.setTime(formatter.parse(creation_date));
        return driverService.findAllDriversCreatedAfterDate(date);
    }

    @PostMapping(value = "/driver/create", consumes = "application/json")
    public void saveDriver(@RequestBody Driver driver) {
        driverService.saveDriver(driver);
    }
}
